
import React from 'react';
import {ScrollView, View} from 'react-native';

import Basicjs from './pages/BasicJs';
import ReactnativeSvg from './pages/ReactnativeSvg';

const App = () => {
  return (
    <View>
      <ScrollView>
        {/* <Basicjs /> */}
        <ReactnativeSvg />
      </ScrollView>
    </View>
  );
};

export default App;