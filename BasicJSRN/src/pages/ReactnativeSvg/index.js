import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

import IllustrationMyApp from '../../assets/image/a.svg';

const ReactnativeSvg = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.Texttittle}>Materi ReactNativeSVG</Text>
      <View style={styles.kotak}>
        <IllustrationMyApp width={244} height={144} />
      </View>
    </View>
  );
};

export default ReactnativeSvg;

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  Texttittle: {
    textAlign: 'center',
  },
  kotak: {
    alignItems: 'center',
    marginTop:20,
  },
});
