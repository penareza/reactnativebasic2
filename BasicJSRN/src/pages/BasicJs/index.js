import React from 'react';
import {StyleSheet, View, Text} from 'react-native';

const Basicjs = () => {
  // primitif
  const nama = 'reza';
  let Age = 32;

  // complex
  const hewan = {
    nama: 'aku',
    jenis: 'kucing',
    usia: 2,
    apakahhewanlokal: true,
    warna: 'kuning',
    orangtua: {
      jantan: 'noel',
      betina: 'eni',
    },
  }; // object

  const SapaOrang = (name, age) => {
    return console.log('Hello ${nama} Usia anda ${age}');
  }; // function
  SapaOrang('Reza', 25);
  const namaOrg = ['Reza', 'Abyan', 'Ummi Yaya', 'Bobby']; // object - array

  typeof namaOrg; // object

  //  if(hewan.nama === 'weow'){
  //    console.log('ini hewan saya');
  //  } else {
  //    console.log('Hewan siapa ini?')
  //  }

   const Sapahewan = (objecthewan) => {
    let hasilsapa = '';
    if (objecthewan.nama === 'aku') {
      hasilsapa = 'ini hewan saya';
    } else {
      hasilsapa = 'Hewan siapa ini?';
    }

    return hasilsapa;
  };

  return (
    <View style={styles.container}>
      <Text style={styles.Texttitle}>Basic Javascript</Text>
      <Text>{nama}</Text>
      <Text>{Age}</Text>

      <Text>{Sapahewan(hewan)}</Text>
      <Text>{namaOrg[0]}</Text>
      <Text>{namaOrg[1]}</Text>
      <Text>{namaOrg[2]}</Text>
      {namaOrg.map(orang => (
         <Text>{orang}</Text> 
      ))}

    </View>
  );
};

export default Basicjs;

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },

  Texttitle: {
    textAlign: 'center',
  },
});
